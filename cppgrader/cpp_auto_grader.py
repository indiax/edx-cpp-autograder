import time
import logging
import json
import urllib2
import xqueue_util as util
import settings
import urlparse
import project_urls
import os
import subprocess
import urllib

log = logging.getLogger(__name__)

QUEUE_NAME = settings.QUEUE_NAME

def each_cycle():
    print('[*]Logging in to xqueue')
    session = util.xqueue_login()
    success_length, queue_length = get_queue_length(QUEUE_NAME, session)
    if success_length and queue_length > 0:
        success_get, queue_item = get_from_queue(QUEUE_NAME, session)
        print(queue_item)
        success_parse, content = util.parse_xobject(queue_item, QUEUE_NAME)
        if success_get and success_parse:
            #grade(content)
	    mark, out, err = grade(content)

            feedback = out
	    if not err == "" :
	    	feedback += "\n<font color='red'> Error message(s) : </font>\n" + err

            feedback = feedback.replace("\n", "<br />")
            feedback = feedback.replace("\n", "<br />")
            content_header = json.loads(content['xqueue_header'])
            content_body = json.loads(content['xqueue_body'])
            xqueue_header, xqueue_body = util.create_xqueue_header_and_body(content_header['submission_id'], content_header['submission_key'], True, mark, '<p><emph>'+feedback+'</emph></p>', 'reference_dummy_grader')
            (success, msg) = util.post_results_to_xqueue(session, json.dumps(xqueue_header), json.dumps(xqueue_body),)
            if success:
                print("successfully posted result back to xqueue")


def grade(content):
    xqueue_body = json.loads(content['xqueue_body'])
    grader_payload = eval(xqueue_body['grader_payload'])
    anonymous_student_id = eval(xqueue_body['student_info'])['anonymous_student_id']
    reference_program = os.path.basename(grader_payload['reference_program'])
    inputs_to_program =  grader_payload['inputs_to_program']
    outputs_to_program = grader_payload['expected_outputs']
    inputs =""
    outputs =""

    if not os.path.isfile(reference_program):
        urllib.urlretrieve (grader_payload['reference_program'], reference_program)
    for inputfileurl in inputs_to_program:
        inputs += os.path.basename(inputfileurl) + ","
        if not os.path.isfile(os.path.basename(inputfileurl)):
            urllib.urlretrieve (inputfileurl, os.path.basename(inputfileurl))
    for outputfileurl in outputs_to_program:
        outputs += os.path.basename(outputfileurl) + ","
        if not os.path.isfile(os.path.basename(outputfileurl)):
            urllib.urlretrieve (outputfileurl, os.path.basename(outputfileurl))


    mark = 0
    files = json.loads(content['xqueue_files'])
    for filename, filepath in files.iteritems():
        if "http://" in filepath or "https://" in filepath:
            urllib.urlretrieve(filepath, filename)
            studentfile =  filename
        command = "python eval_assign.py -r "+ reference_program + " -s "+ studentfile + " -k " + anonymous_student_id + " -i "+ inputs[:-1] + " -e " + outputs[:-1]
	process = subprocess.Popen(command, bufsize=-1,
                               shell=True, close_fds=True,
                               preexec_fn=os.setsid,
                               stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE)
        (out, err) = process.communicate()
	mark = out.splitlines()[-1]
	mark = float(mark.replace("Total Score : ",""))
        if "http://" in filepath[0] or "https://" in filepath[0]:
            os.remove(studentfile)	
    return mark, out, err


def get_from_queue(queue_name,xqueue_session):
    """
        Get a single submission from xqueue
        """
    try:
        success, response = util._http_get(xqueue_session,
                                           urlparse.urljoin(settings.XQUEUE_INTERFACE['url'], project_urls.XqueueURLs.get_submission),
                                           {'queue_name': queue_name})
    except Exception as err:
        return False, "Error getting response: {0}".format(err)
    
    return success, response



def get_queue_length(queue_name,xqueue_session):
    """
        Returns the length of the queue
        """
    try:
        success, response = util._http_get(xqueue_session,
                                           urlparse.urljoin(settings.XQUEUE_INTERFACE['url'], project_urls.XqueueURLs.get_queuelen),
                                           {'queue_name': queue_name})
        
        if not success:
            return False,"Invalid return code in reply"
    
    except Exception as e:
        log.critical("Unable to get queue length: {0}".format(e))
        return False, "Unable to get queue length."
    
    return True, response

try:
    logging.basicConfig()
    while True:
        each_cycle()
        time.sleep(2)
except KeyboardInterrupt:
    print '^C received, shutting down'
