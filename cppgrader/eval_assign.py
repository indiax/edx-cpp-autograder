#!/usr/bin/env python

################################################################################
# Perform automatic evaluation of a set of student assignments
################################################################################

__author__  = "Anindya Sen (anindya@cse.iitb.ac.in)"
__version__ = "$Revision: 1.0 $"
__date__    = "$Date: 2014/10/09$"
__license__ = "Python"

import sys, shutil
import difflib
import subprocess, os, signal
import math, re, time
from optparse import OptionParser
from xml.dom import minidom

# for implementing process timeout
class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm


def parse_cmd_line() :

    parser = OptionParser()
    parser.add_option("-i", "--inputs", dest="inputs", type="string", 
                      help = "comma-separated list of filenames each containing\
                              a single testcase to be provided to program via stdin")
    
    parser.add_option("-e", "--expected_outputs", dest="expected_outputs", 
                      type="string", default="",
                      help = "comma-separated (optional) list of filenames each\
                              containing the output of the corresponding\
                              testcase provided in inputs")

    parser.add_option("-k", "--student_key", dest="student_key", type="string", 
                      help = "unique identifier for a student")
    
    parser.add_option("-r", "--reference_src", dest="ref_src", type="string", 
                      help = "file containing correct program")
    
    parser.add_option("-s", "--submitted_src", dest="sub_src", type="string", 
                      help = "file containing program submitted by student")
    
    parser.add_option("-t", "--timeout", dest="timeout", type="int", default="30",
                      help = "wall-clock time before a program is aborted (default = 30s)")

    (options, args) = parser.parse_args()
    return options



################################################################################
# If the marks for a particular block is missing, the autograder assigns to it 
# a default value of 1.
################################################################################

def scan_blocks(ref_src) :

    block_delimiter = "\n *////[^\n]*(BEGIN|END)([^\n]*)DONT_ERASE_([^\n]*)\n"
    block_matches   = re.findall(block_delimiter, ref_src)

    block_info = []
    max_score = max_bonus_score = 0

    for block in block_matches :
        
        # check if this is a bonus question
        if block[1].find('BONUS') == -1 :
            bonus_block = False
        else :
            bonus_block = True

        # get block_id and marks (default = 1)
        block_id_str, marks_str = block[2].split('_') if \
                                       '_' in block[2]  else (block[2], '1')
        block_id = int(block_id_str)
        marks    = int(marks_str)

        if block[0] == 'BEGIN' :
            previous_block_id = block_id
            begin_block = "\n *////[^\n]*BEGIN:[^\n]*" + block_id_str + "[^\n]*\n"

        elif block[0] == 'END' :
            # make sure that all begin/end occur in pairs
            if not block_id == previous_block_id :
                raise SyntaxError("Unmatched BEGIN/END comments in " +
                                  "reference file (Block ID : " + 
                                  str(previous_block_id) + ")")

            end_block = "\n *////[^\n]*END:[^\n]*"  + block_id_str + "[^\n]*\n"

            # capture block text
            block_text = re.search(begin_block + "(.*)" + end_block, ref_src, re.DOTALL).group(1)
            block_info.append((block_id, (block_text, marks, bonus_block)))
    
    # final block should be an 'END' block
    if block_matches[-1][0] == 'BEGIN' :
        raise SyntaxError("Unmatched BEGIN/END comments in " +
                          "reference file (Block ID : "      + 
                          str(block_id) + ")")

    return dict(block_info)



################################################################################
# Given a list of comma-separated input filenames and another (possibly empty)
# list of comma-separated output filenames, generate a list of testcases.
################################################################################

def generate_testcases(inputs, outputs, ref_src, target_exe) :

    input_file_list = inputs.split(',')
    num_testcases   = len(input_file_list)

    # create a list of (file, i/p, o/p), one for each testcase
    try :
        testcases = []
        for f in input_file_list :
            stdin = open(f,'r').read()
            testcases.append([f, stdin])
    except IOError, why :
        print str(why)
        raise

    # expected output files already specified, no need to regenerate
    if not outputs == "" :

        # number of input and output files must be equal!
        output_file_list = outputs.split(',')
        if not len(output_file_list) == num_testcases :
            sys.stderr.write("Error : The number of input and output " + 
                                                 "files do not match\n")
            raise Exception

        # read, in order, each expected output file
        try :
            i = 0
            for f in output_file_list :
                testcases[i].append(open(f,'r').read())
                i += 1

        except IOError, why :
            print str(why)
            raise
    
    # expected outputs not provided by user,
    # generate it by compiling and running ref_src
    else : 
        (stdout, stderr, comp_status) = compile_code(ref_src, target_exe)
        
        if not comp_status == 0:
            sys.stderr.write("Error : Failed to compile reference file\n")
            raise Exception
        
        for testcase in testcases :
            (stdout, stderr, rcode) = run_code(target_exe, testcase[1], "", 100)
            testcase.append(stdout)

    return testcases



################################################################################
# Replace a block (using block_id as key) from destination with correct_block
# string.  Function expects the block to be replaced to be in a prescribed
# format and raises exception if that condition is violated.
################################################################################

def swap_code(destination, correct_block, block_id) :

    block_str_id = str(block_id)
    if block_id < 10 :
        block_str_id = "0" + block_str_id
    
    end_intro_str   = " *////[^\n]*BEGIN[^\n]*DONT_ERASE_" + block_str_id + "[^\n]*\n"
    begin_concl_str =   " *////[^\n]*END[^\n]*DONT_ERASE_" + block_str_id + "[^\n]*\n"

    intro_match = re.search(  ".*" + end_intro_str, destination, re.DOTALL)
    concl_match = re.search(begin_concl_str + ".*", destination, re.DOTALL)

    # check for beginning of block in student implementation
    if intro_match :
        intro = intro_match.group(0)
    else :
        raise SyntaxError("Error : Unable to find start of block " \
                          + str(block_id) + " in student implementation\n")

    # check for end of block in student implementation
    if concl_match :
        concl = concl_match.group(0)
    else :
        raise SyntaxError("Error : Unable to find end of block " \
                          + str(block_id) + " in student implementation\n")

    # swapped code = intro + correct_block + concl
    return intro + correct_block + concl



################################################################################
# Function to replace all but one block (specified by block_id) in destination
# with a matching correct block (minus the block delimiters) from the reference
# implementation.
# 
# block_info contains the blocks from reference implementation.
#
# If a block in destination does NOT appear in the prespecified format, that block
# is ignored i.e. no replacement is performed, and an error message is sent to
# stderr.
################################################################################

def swap_all_but_one_blocks(destination, block_info, block_id) :

    swapped = destination

    for i in range(2,len(block_info)+1) :
        if i == block_id : continue
        
        try :
            swapped = swap_code(swapped, block_info[i][0], i)
        except SyntaxError, why :
            sys.stderr.write(str(why))

    return swapped



################################################################################
# Compile src written in c++ using the g++ compiler.  Instead of writing src to
# a file and passing the filename to g++, src is directly sent to compile
# command via stdin.  Function returns the return code of the compile command
# along with its stdout and stderr output.
################################################################################

def compile_code(src, target_exe) :

    cmd = "g++ -Wall -o " + target_exe + " -x c++ -"
    process = subprocess.Popen(cmd, bufsize=-1,
                               shell=True, close_fds=True,
                               preexec_fn=os.setsid,
                               stdin  = subprocess.PIPE,
                               stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE)

    (c_stdout, c_stderr) = process.communicate(input=src)

    # compilation status
    c_status =  process.returncode
    return (c_stdout, c_stderr, c_status)



################################################################################
# Program executes target_exe with input provided by std_in and returns stdout,
# stderr as strings along with the return code.
#
# target_exe is run with std_input feeding data to its stdin.
#
# The final output is compared with expected_output and output is considered
# correct if it matches expected_output EXACTLY.
#
# Function returns :
# out : output produced by running program
# err : error message generated by running program
# return code :
#    1 : output is correct
#    0 : output is incorrect
################################################################################

def run_code(target_exe, std_input, expected_output, timeout) :

    process = subprocess.Popen(target_exe, bufsize=-1,
                               shell=True, close_fds=True,
                               preexec_fn=os.setsid,
                               stdin  = subprocess.PIPE,
                               stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE)
    
    # monitor execution time 
    signal.signal(signal.SIGALRM, alarm_handler)
    signal.alarm(timeout)

    # http://stackoverflow.com/questions/1191374/subprocess-with-timeout
    try :
        (out, err) = process.communicate(std_input)
        run_code   = process.returncode
        signal.alarm(0)  # reset alarm
    except Alarm :
        (out, err) = ("", "Error : Process Timeout\n")
        run_code   = process.returncode
        os.killpg(process.pid, signal.SIGKILL) 
    
    #s = difflib.SequenceMatcher(lambda x: x in " \t\n", out, expected_output)
    #match = s.ratio()
    #if match == 1.0 :

    #check if output matches expected output
    if out == expected_output :
        # program compiled successfully and output is correct
        return (out, err, 1)
    else :
        # program compiled successfully but output is incorrect
        return (out, err, 0)
    return (out, err, run_code)



################################################################################
# evaluate student submission one block at a time
################################################################################

def eval_student_submission(sub_src, block_info, target_exe, testcases, timeout) :

    score = 0

    for block_id in range(2,len(block_info)+1) :
        test_block_str = "Testing Block " + str(block_id) + " :"
        print test_block_str

        exec_code = swap_all_but_one_blocks(sub_src, block_info, block_id)
        
        (out, err, comp_status) = compile_code(exec_code, target_exe)
        
        # program failed to compile
        if not comp_status == 0:
            sys.stderr.write("  Error : Failed to compile student submission\n")
            sys.stderr.write(err)
            continue

        # run generated target_exe on each testcase
        counter = 0
        for testcase in testcases :
            (out, err, status) = run_code(target_exe, testcase[1], testcase[2],
                                                                       timeout)

            if status == 1 :
                part_score = block_info[block_id][1]/(1.0 * len(testcases))
                score     += part_score
                print "  Testcase = '" + testcase[0] + "', Score = %.2f" %part_score

            elif status == 0 :
                if counter == 0 :
                    sys.stderr.write(test_block_str + "\n")
                testcase_score_str = "  Testcase = '" + testcase[0] + "', Score = 0.00"
                print testcase_score_str
                sys.stderr.write(testcase_score_str + "\n")
                sys.stderr.write("    Error : Output does not match expected output\n")
                sys.stderr.write(err)

            counter+=1
    
    return score



if __name__ == "__main__":
    
    options = parse_cmd_line()
    target_exe = "/tmp/cs101_" + options.student_key + ".out"

    # read program files
    try :
        ref_src   = open(options.ref_src,'r').read()
        sub_src   = open(options.sub_src,'r').read()

    except IOError, why :
        print str(why)
        raise

    # compute information for each block (including bonus blocks)
    block_info = scan_blocks(ref_src)
    max_bonus_score = sum([val[1] for key,val in block_info.iteritems()]) 
    max_score = sum([val[1] for key,val in block_info.iteritems() 
                                                     if val[2] == False]) 

    # generate list of testcases
    testcases = generate_testcases(options.inputs, options.expected_outputs,
                                                        ref_src, target_exe)

    # evaluate submitted source
    score = eval_student_submission(sub_src, block_info, target_exe, 
                                         testcases, options.timeout)
    print "\nTotal Score : %.2f" % score
