// Error : Incorrect initialization in Block 2 
// Error(?) :  Tampering with block delimiter ('BGIN' instead of 'BEGIN')
// However, since the rest of the code is correct, student still manages to get
// full marks!  This will probably NOT generalize.

#include <iostream>
#include <cmath>
using namespace std;

// Please write ALL code, except functions that you may define yourself,  within
// the "BEGIN-END" blocks given below.  A "BEGIN-END" block is identified by the
// following signature :
//
// "//// BEGIN: Some string DONT_ERASE_xx[_yy]"
// "  Your code goes HERE"
// "//// END: Some other string DONT_ERASE_xx_[_yy]"
//
// where "xx" is a unique integer identifier for the block and "yy" is the
// number of points allocated for the block (default value = 1).  
//
// If "Some string" in the BEGIN line of block contains the word "BONUS" (all
// caps), then the block is treated as corresponding to a "bonus problem".
// Apart from the fact that bonus problems help you earn EXTRA points, they are
// NO different from other problems.
//
// The FIRST block (BLOCK 1) carries no points and is a placeholder for your
// personal information as well as a place to declare functions defined by you
// (see end of file).
// 
// Feel free to write your own functions ("USER-DEFINED FUNCTIONS") at the
// designated location at the bottom of this file.  You may call these functions
// from within the blocks.  Make sure that you declare those functions inside
// BLOCK 1.  Also make sure that the addition of these functions don't cause
// compilation errors.
//
// WARNING : 
// (1) DO NOT tamper with the block delimiters under ANY circumstance.  This
//     will simply cause the autograder to fail on ALL test cases.
//
// (2) Any changes to code outside the blocks (except "USER-DEFINED FUNCTIONS"
//     at the bottom) MAY cause the autograder to NOT evaluate your code

//     question.
//
// (3) Your FINAL submission should have ZERO 'cout' statements added by you.
//     Make sure that you REMOVE any 'cout' statement added to aid debugging
//     when submitting.


//// ---------------------------------------------------------------------------
//// BEGIN: Fill your details as comments below DONT_ERASE_01_0
//// Name:
//// Roll number:
////
//// ADD declarations for USER-DEFINED functions (see the end of this file) here :
//// DECL_1 (uncomment this line)
//// DECL_2 (uncomment this line)
//// DECL_3 (uncomment this line)
//// etc.
////
//// END: Fill your details as comments above DONT_ERASE_01_0
//// ---------------------------------------------------------------------------



// Program to calculate average and standard deviation of marks scored in a
// quiz, by students of a class program is stored in the file
// marks_statistics.cpp It works for at most 600 students

float compute_average(int marks[], int num_students);
float compute_std_dev(int marks[], int num_students, float average);

int main()
{
    int marks[600], count, num_students;
    float average, std_dev;

    //// Read and validate the value of NumStudents,
    //// exit if value is less than 1, or more than 600
    cout << "Give number of students: ";
    cin >> num_students;
    if (num_students < 1 || num_students > 600){
        cout << "Invalid Value for number of students: ";
        cout << num_students << endl;
        cout <<"Exiting program with error code 1" << endl;
        return -1;
    }
    
    //// Read marks for each student
    for (count = 0; count < num_students; count++)
    {  
        cout << "Give marks of student " << count+1 << ": ";
        cin >> marks[count];
    }

    average = compute_average(marks, num_students);

    std_dev = compute_std_dev(marks, num_students, average);

    //// Output results 
    cout << "Statistics of Quiz 1 marks of the class" << endl;
    cout << "Number of students is:" <<num_students <<  endl;
    cout << "Average Marks: " << average <<  endl;
    cout << "Standard Deviation of marks: " << std_dev <<  endl;

    return 0;
}


float compute_average(int marks[], int num_students)
{
//// ---------------------------------------------------------------------------
//// BEGIN: Write code to calculate average (marks = 2) DONT_ERASE_02_02
//// Calculate sum of all marks,and the average
    int count;
    float sum, average;
    sum = 1.0;

    for (count = 0; count < num_students; count++)
    {  
        sum = sum + marks[count];
    }

    average = sum/num_students;
    return average;
//// END: Code to calculate average DONT_ERASE_02_02
//// ---------------------------------------------------------------------------
}


float compute_std_dev(int marks[], int num_students, float average)
{
//// ---------------------------------------------------------------------------
//// BGIN: This is a BONUS question with default marks = 1.  DONT_ERASE_03
//// Now calculate standard deviation of marks
    int count;
    float sum = 0.0, diff;
    float std_dev;

    for (count = 0; count < num_students; count++)
    {  
        diff = marks[count] - average;
        sum = sum + diff * diff;
    }

    std_dev = sqrt(sum/num_students);

    return std_dev;
//// END: This is a BONUS question.  DONT_ERASE_03
//// ---------------------------------------------------------------------------
}


//// ---------------------------------------------------------------------------
//// USER-DEFINED FUNCTIONS : Define all your functions BELOW this line
//// DO NOT forget to declare them inside BLOCK 1 (see beginning of file)
